import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import Logo from "../elements/Logo";
import ImportantNotice from "../elements/ImportantNotice";
import { useNavigation } from "@react-navigation/native";

const WelcomeScreen = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const navigation = useNavigation();

  useEffect(() => {
    // Loading delay
    const timer = setTimeout( () => {
      setIsLoading(false);
      setShowModal(true);
    }, 3000);

    return () => clearTimeout(timer);
  }, []);

  const handleAgree = () => {
    setShowModal(false);
    navigation.navigate("WelcomePage");
  };

  return (
    <View style={styles.background}>
      <Logo isLoading={isLoading} />
      <ImportantNotice showModal={showModal} handleAgree={handleAgree} />
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default WelcomeScreen;
