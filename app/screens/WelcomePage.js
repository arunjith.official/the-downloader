import React, { useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert, Platform } from 'react-native';
import PlatformSelector from '../elements/PlatformSelector';
import URLPrompt from '../elements/URLPrompt';
import { Ionicons } from '@expo/vector-icons';
import * as FileSystem from 'expo-file-system';
import DownloadButton from '../elements/DownloadButton';

const WelcomePage = () => {
  const [selectedPlatform, setSelectedPlatform] = useState('');
  const [url, setUrl] = useState('');

  const handlePlatformSelect = (platform) => {
    setSelectedPlatform(platform);
  };

  const handleUrlChange = (text) => {
    setUrl(text);
  };

  const downloadFile = () => {
    if (!url || !selectedPlatform) {
      Alert.alert('Missing Information', 'Please enter a URL and select a platform.');
      return;
    }
  
    let downloadUrl = `https://astonishing-concha-93b501.netlify.app/download?url=
    ${encodeURIComponent(url)}&platform=${encodeURIComponent(selectedPlatform)}`;
  
    if (Platform.OS === 'web') {
      window.open(downloadUrl, '_blank');
    } else {
      const fileName = 'downloadedFile';
      const destination = `${FileSystem.documentDirectory}${fileName}`;
  
      FileSystem.downloadAsync(downloadUrl, destination)
        .then(({ uri }) => {
          console.log('File downloaded at:', uri);
  
          // Show success message or perform any further actions
          Alert.alert('Download Complete', 'The file has been downloaded successfully.');
        })
        .catch((error) => {
          console.error('Error downloading file:', error);
          Alert.alert('Download Error', 'There was an error while downloading the file. Please try again.');
        });
    }
  
    // Reset the input fields after initiating the download
    setUrl('');
    setSelectedPlatform('');
  };
  

  return (
    <View style={styles.background}>
      <View style={styles.container}>
        <Text style={styles.title}>Sixbit Downloader!</Text>
        <PlatformSelector selectedPlatform={selectedPlatform} 
        handlePlatformSelect={handlePlatformSelect} />
        {selectedPlatform && (
          <URLPrompt url={url} handleUrlChange={handleUrlChange} />
        )}
        {url && (
          <DownloadButton handleDownload={downloadFile}/>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    width: '80%',
    padding: 20,
    backgroundColor: '#ffffff',
    borderRadius: 10,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  downloadButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#6c63ff',
    borderRadius: 10,
    paddingVertical: 12,
  },
  downloadButtonText: {
    color: '#ffffff',
    fontSize: 18,
    marginLeft: 10,
  },
});

export default WelcomePage;
