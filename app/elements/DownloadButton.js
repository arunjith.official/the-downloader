import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";

const DownloadButton = ({ handleDownload }) => {
  return (
    <TouchableOpacity style={styles.button} onPress={handleDownload}>
      <Text style={styles.buttonText}>Download</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    marginTop: 20,
    backgroundColor: "#2196F3",
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderRadius: 8,
    alignItems: "center",
  },
  buttonText: {
    color: "#ffffff",
    fontSize: 16,
    fontWeight: "bold",
  },
});

export default DownloadButton;
