import React from "react";
import { StyleSheet, View, Text, Button, Modal } from "react-native";

const ImportantNotice = ({ showModal, handleAgree }) => {
  return (
    <Modal animationType="slide" transparent={true} visible={showModal}>
      <View style={styles.modalContainer}>
        <View style={styles.modalContent}>
          <Text style={styles.header}>Important Info</Text>
          <Text style={styles.description}>
            This video downloader app is not responsible for 
            copyright issues arising from downloaded content. Please be responsible and refrain from piracy.
          </Text>
          <Button onPress={handleAgree} title="Agree" />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalContent: {
    backgroundColor: "#fff",
    borderRadius: 6,
    padding: 20,
    alignItems: "center",
  },
  header: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
  description: {
    marginBottom: 20,
    textAlign: "center",
  },
});

export default ImportantNotice;
