import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

const QualitySelector = ({ qualities, selectedQuality, handleQualitySelect }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Select Quality:</Text>
      {qualities.map((quality) => (
        <TouchableOpacity
          key={quality}
          style={[
            styles.qualityButton,
            selectedQuality === quality ? styles.selectedQualityButton : null,
          ]}
          onPress={() => handleQualitySelect(quality)}
        >
          <Text
            style={[
              styles.qualityText,
              selectedQuality === quality ? styles.selectedQualityText : null,
            ]}
          >
            {quality}
          </Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
  },
  qualityButton: {
    padding: 10,
    marginVertical: 5,
    backgroundColor: "#e0e0e0",
    borderRadius: 5,
  },
  selectedQualityButton: {
    backgroundColor: "#2196F3",
  },
  qualityText: {
    color: "black",
    fontSize: 16,
  },
  selectedQualityText: {
    color: "white",
    fontWeight: "bold",
  },
});

export default QualitySelector;
