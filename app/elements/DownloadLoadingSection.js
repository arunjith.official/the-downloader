import React from "react";
import { View, Text } from "react-native";

const DownloadLoadingSection = ({ loading }) => {
  return loading ? <Text>Loading...</Text> : null;
};

export default DownloadLoadingSection;
