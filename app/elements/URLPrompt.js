import React from "react";
import { View, TextInput } from "react-native";

const URLPrompt = ({ url, handleUrlChange }) => {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Enter video URL"
        value={url}
        onChangeText={handleUrlChange}
      />
    </View>
  );
};

const styles = {
  container: {
    marginBottom: 20,
  },
  input: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    paddingHorizontal: 10,
  },
};

export default URLPrompt;
