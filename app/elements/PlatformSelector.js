import React from "react";
import { View, Button } from "react-native";


const PlatformSelector = ({ selectedPlatform, handlePlatformSelect }) => {
  return (
    <View style={styles.container}>
      <Button
        title="YouTube"
        onPress={() => handlePlatformSelect("youtube")}
        color={selectedPlatform === "youtube" ? "#007bff" : null }
      />
      <Button
        title="Instagram"
        onPress={() => handlePlatformSelect("instagram")}
        color={selectedPlatform === "instagram" ? "#007bff" : null}
      />
      <Button
        title="Other"
        onPress={() => handlePlatformSelect("other")}
        color={selectedPlatform === "other" ? "#007bff" : null}
      />
    </View>
  );
};

const styles = {
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 20,
  },
};

export default PlatformSelector;
