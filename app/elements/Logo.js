import React from "react";
import { StyleSheet, View, Text } from "react-native";
import * as Animatable from "react-native-animatable";

const Logo = () => {
  return (
    <View style={styles.logoContainer}>
      <Animatable.Text
        animation="bounceIn"
        style={styles.logoText}
        duration={1500}
        delay={500}
      >
        Sixbit Downloader
      </Animatable.Text>
    </View>
  );
};

const styles = StyleSheet.create({
  logoContainer: {
    alignItems: "center",
  },
  logoText: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#fff",
    marginBottom: 20,
  },
});

export default Logo;
